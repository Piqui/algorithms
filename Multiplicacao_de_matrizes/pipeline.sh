#!/bin/bash

# compilando
echo "Compilando ..."
gcc C_file/strassen_s.c -Wall -o bin/sequencial
gcc C_file/strassen_m.c -Wall -fopenmp -o bin/multi
mpicc.openmpi C_file/strassen_a.c -o bin/all

echo "Executando ..."
for i in "$@"
do
case $i in
  -n=*|--number=*)
  NUMBER="${i#*=}"
  shift
  ;;
  -s=*|--sequencial=*)
  SEQUENCIAL="${i#*=}"
  echo "Sequencial ..."
  (time(./bin/sequencial $NUMBER)) 2>> times_system/time_sequencial.txt
  (echo "n = $NUMBER") >> times_system/time_sequencial.txt
  ;;
  -m=*|--multithreads=*)
  MULTI="${i#*=}"
  echo "Multiprocess ..."
  (time(./bin/multi $NUMBER)) 2>> times_system/time_multi.txt
  (echo "n = $NUMBER") >> times_system/time_multi.txt
  ;;
  -a=*|--all-action=*)
  ALL="${i#*=}"
  echo "Multiprocess and multiboard ..."
  (time(mpirun -np 4 bin/all $NUMBER)) 2>> times_system/time_all.txt
  (echo "n = $NUMBER") >> times_system/time_all.txt
  ;;
  --default)
  DEFAULT=YES
  shift
  ;;
  *)
;;
esac
done
echo "Finish !!! Sucess!!!"
