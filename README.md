# Wellcome to the algorithms project !!!
## How execute matrices test bench
* To execute matrices test bench you can get inside Multiplicacao_de_matrizes and execute bash file pipeline with the command *bash pipeline.sh* with the flags -s -m -a:
 * -n='num of matrix dimension';    

 * -s=True you execute sequencial multiplication;

 * -m=True you execute multithread single board multiplication;    

 * -a=True you execute multithread and multiboard multiplication;    
*example:* *bash pipeline.sh -n='number of matrix dimension' -s=True -m=True -a=True*

#### Obs: one flag isn't exclude another flag. If you don't want execute one of flags you only don't put it in command. Example: bash pipeline.sh -n=10000 -s=True. Only execute a sequencial multiplication.

## Resuts
* To see the results you can open the files in time or time_system directory.
